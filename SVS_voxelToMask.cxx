#include <itkSpatialObjectToImageFilter.h>
#include <itkGroupSpatialObject.h>
#include <itkBoxSpatialObject.h>
#include <itkImageFileWriter.h>
#include <itkImageFileReader.h>
#include <itkSimpleFilterWatcher.h>

#include "gdcmReader.h"
#include "gdcmImageReader.h"
#include "gdcmImageWriter.h"
#include "gdcmCSAHeader.h"
#include "gdcmAttribute.h"
#include "gdcmPrivateTag.h"

#include <math.h>

#include <vnl/vnl_vector_fixed.h>
#include <vnl/vnl_cross.h>

template <typename PointType>
int
getBoundingPoints(char *filename, PointType &c0, PointType &c1, PointType &c2,  PointType &c3,  PointType &c4 )
{
  gdcm::Reader reader; // Do not use ImageReader
  reader.SetFileName( filename );
  if( !reader.Read() )
    {
    std::cerr << "Failed to read: " << filename << std::endl;
    return 1;
    }

  //Load in the shadow header
  gdcm::CSAHeader csa;
  const gdcm::DataSet& ds = reader.GetFile().GetDataSet();
  const gdcm::PrivateTag &t1 = csa.GetCSAImageHeaderInfoTag();

  if( ds.FindDataElement( t1 ) )
  {
    csa.LoadFromDataElement( ds.GetDataElement( t1 ) );
  }
  else
  {
    std::cerr << "error Loading siemens shadow header from : " << filename << std::endl;
    return EXIT_FAILURE;
  }

  gdcm::CSAElement csaElement;
  vnl_vector_fixed<double,3> v1,v2,v3;
  
  if ( csa.FindCSAElementByName( "ImageOrientationPatient" ) )
  {
    csaElement = csa.GetCSAElementByName( "ImageOrientationPatient" );
    gdcm::Element<gdcm::VR::DS, gdcm::VM::VM6> el;
    el.Set( csaElement.GetValue() );
    v1[0] = el.GetValue(0);
    v1[1] = el.GetValue(1);
    v1[2] = el.GetValue(2);
    v2[0] = el.GetValue(3);
    v2[1] = el.GetValue(4);
    v2[2] = el.GetValue(5);
    v3 = vnl_cross_3d (v1,v2);
  }
  else
  {
    std::cerr << "No ImageOrientationPatient" << std::endl;
    return EXIT_FAILURE;
  }    

  //Normalize v1,v2,v3 (shouldn't be needed)
  v1 = v1.normalize();
  v2 = v2.normalize();
  v3 = v3.normalize();
  
  //Get dims
  int dims[2] = {};
  if( csa.FindCSAElementByName( "Columns" ) )
  {
    const gdcm::CSAElement &csael = csa.GetCSAElementByName( "Columns" );
    gdcm::Element<gdcm::VR::IS, gdcm::VM::VM1> el;
    el.Set( csael.GetValue() );
    dims[0] = el.GetValue();
  }

  if( csa.FindCSAElementByName( "Rows" ) )
  {
    const gdcm::CSAElement &csael2 = csa.GetCSAElementByName( "Rows" );
    gdcm::Element<gdcm::VR::IS, gdcm::VM::VM1> el2;
    el2.Set( csael2.GetValue() );
    dims[1] = el2.GetValue();
  }

  double spacing[2] = { 1. , 1. };
  bool spacingfound = false;
  if( csa.FindCSAElementByName( "PixelSpacing" ) )
  {
    const gdcm::CSAElement &csael3 = csa.GetCSAElementByName( "PixelSpacing" );
    if( !csael3.IsEmpty() )
    {
      gdcm::Element<gdcm::VR::DS, gdcm::VM::VM2> el3;
      el3.Set( csael3.GetValue() );
      spacing[0] = el3.GetValue(0);
      spacing[1] = el3.GetValue(1);
      spacingfound = true;
    }
  }

  if( !spacingfound )
  {
    std::cerr << "Problem with PixelSpacing" << std::endl;
    return EXIT_FAILURE;
  }

  //get position
  vnl_vector_fixed<double,3> corner;
  if( csa.FindCSAElementByName( "ImagePositionPatient" ) )
  {
    const gdcm::CSAElement &csael3 = csa.GetCSAElementByName( "ImagePositionPatient" );
    if( !csael3.IsEmpty() )
    {
      gdcm::Element<gdcm::VR::DS, gdcm::VM::VM3> el3;
      el3.Set( csael3.GetValue() );
      corner[0] = el3.GetValue(0);
      corner[1] = el3.GetValue(1);
      corner[2] = el3.GetValue(2);
    }
    else
    {
      std::cerr << "Empty ImagePositionPatient Element!" << std::endl;
      return EXIT_FAILURE;
    }
  }
  else
  {
    std::cerr << "Can't find ImagePositionPatient Element!" << std::endl;
    return EXIT_FAILURE;
  }
  
  //GetSliceThickness
  double sliceThickness=0;
  if( csa.FindCSAElementByName( "SliceThickness" ) )
  {
    const gdcm::CSAElement &csael3 = csa.GetCSAElementByName( "SliceThickness" );
    if( !csael3.IsEmpty() )
    {
      gdcm::Element<gdcm::VR::DS, gdcm::VM::VM1> el3;
      el3.Set( csael3.GetValue() );
      sliceThickness = el3.GetValue();
    }
  }
  else
  {
    std::cerr << "Can't find SliceThickness Element!" << std::endl;
    return EXIT_FAILURE;
  }
 
  //Find the points of the bounding box...
  /*
  c0 = cbot(1,:)
  c1 = cbot(2,:)
  c2 = cbot(4,:)
  c3 = ctop(1,:)
  */
  vnl_vector_fixed<double,3> c0v,c1v,c2v,c3v,c4v;

  c0v = corner-(sliceThickness/2.0)*v3; 
  c1v = c0v + dims[1] * spacing[1] * v1; // notice the switch this is only for Spectra
  c2v = c0v + dims[0] * spacing[0] * v2; // notice the switch this is only for Spectra
  c3v = corner+(sliceThickness/2.0)*v3;
  c4v = c3v + dims[1] * spacing[1] * v1 + dims[0] * spacing[0] * v2;

  //Must be a better way, then to use vnl_vectors and intermediates!!
  c0[0]=c0v[0];  c0[1]=c0v[1];  c0[2]=c0v[2];
  c1[0]=c1v[0];  c1[1]=c1v[1];  c1[2]=c1v[2];
  c2[0]=c2v[0];  c2[1]=c2v[1];  c2[2]=c2v[2];
  c3[0]=c3v[0];  c3[1]=c3v[1];  c3[2]=c3v[2];
  c4[0]=c4v[0];  c4[1]=c4v[1];  c4[2]=c4v[2];
  return EXIT_SUCCESS;
}
 
template <typename GroupObjectT, unsigned int Dimension>
int addBoxObject( typename GroupObjectT::Pointer gObj, char * dcmFile, unsigned int value )
{
  typedef itk::BoxSpatialObject< Dimension >        BoxType;

  typename BoxType::Pointer box    = BoxType::New();
  typename BoxType::SizeType sizeArray;
  sizeArray[0] = 1;
  sizeArray[1] = 1;
  sizeArray[2] = 1;
  
  box->SetSize( sizeArray );
  
  typedef typename BoxType::TransformType                 TransformType;
 
  typename TransformType::Pointer transform = TransformType::New();
 
  typedef typename TransformType::InputPointType          InputPointType;
  
  InputPointType p0,p1,p2,p3,p4;
  if (getBoundingPoints(dcmFile,p0,p1,p2,p3,p4) != EXIT_SUCCESS)
  {
    std::cerr << "Something is wrong Getting bounding box for \n\t" << dcmFile << std::endl;
    return EXIT_FAILURE;
  }
  
  //Put it where we want it?
  typename TransformType::ParametersType params;
  params.SetSize(12);

  /**
    the affMat should be...
      
      |   |     |      |    |    |
  T = | p1-p0 p1-p0  p1-p0  p0   |
      |   |     |      |    |    |
      |   0     0      0    1    |
  */
  //Copy the parameters over
  params[0]  = p1[0]-p0[0];
  params[1]  = p2[0]-p0[0];
  params[2]  = p3[0]-p0[0];
  params[3]  = p1[1]-p0[1];
  params[4]  = p2[1]-p0[1];
  params[5]  = p3[1]-p0[1];
  params[6]  = p1[2]-p0[2];
  params[7]  = p2[2]-p0[2];
  params[8]  = p3[2]-p0[2];
  params[9]  = p0[0];
  params[10] = p0[1];
  params[11] = p0[2];

  transform->SetParameters(params);
  
  //Just a test...
  typename TransformType::InputPointType tmpPoint;
  tmpPoint[0]=1.0;tmpPoint[1]=1.0;tmpPoint[2]=1.0;

  typename TransformType::OutputPointType testPoint = transform->TransformPoint(tmpPoint);
  if ( (testPoint - p4).GetVnlVector().two_norm() > 0.001 )
  {
    std::cerr << "Something is wrong with the supplied points... sorry!" << std::endl;
    std::cerr << "p0 --- " << p0 << std::endl;
    std::cerr << "p1 --- " << p1 << std::endl;
    std::cerr << "p2 --- " << p2 << std::endl;
    std::cerr << "p3 --- " << p3 << std::endl;
    std::cerr << "p4 --- " << p4 << std::endl;
    return EXIT_FAILURE;
  }
  else
  {
    std::cerr << "p0 --- " << p0 << std::endl;
    std::cerr << "p1 --- " << p1 << std::endl;
    std::cerr << "p2 --- " << p2 << std::endl;
    std::cerr << "p3 --- " << p3 << std::endl;
    std::cerr << "p4 --- " << p4 << std::endl;
  }
  

  box->SetObjectToWorldTransform( transform );
  box->SetDefaultInsideValue(value);
  box->SetDefaultOutsideValue(0);
  
  gObj->AddSpatialObject(box);

}
 
int main( int argc, char *argv[] )
{
  if( argc < 4 )
    {
    std::cerr << "Usage: " << argv[0] << " templateFile outputimagefile dcmFile1 [dcmFile2 dcmFile3 ...] " << std::endl;
    return EXIT_FAILURE;
    }
 
  typedef short PixelType;
  const unsigned int Dimension = 3;
 
  typedef itk::Image< PixelType, Dimension >        ImageType;
 
  //Read in the templateFile
  typedef itk::ImageFileReader<ImageType>     ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);
	try
	{
		reader->Update();
	}
	catch( itk::ExceptionObject & excp )
	{
		std::cerr << "Exception thrown " << std::endl;
		std::cerr << excp << std::endl;
		return EXIT_FAILURE;
	}
	ImageType::Pointer inImage=reader->GetOutput(); 

  typedef itk::GroupSpatialObject < Dimension >     GroupType;
  GroupType::Pointer group = GroupType::New();

  //Print some info if we supplied multiple dcm files
  if (argc >= 4)
  {
    std::cout <<  " Label :     dcmFile " <<std::endl;
    for (int i = 3; i < argc;++i)
    {
      std::cout << i-2 << "      : " << argv[i] << std::endl;
      addBoxObject<GroupType, Dimension>( group, argv[i], i-2 );
    }
  }
  
  typedef itk::SpatialObjectToImageFilter<
    GroupType, ImageType >   SpatialObjectToImageFilterType;
 
  SpatialObjectToImageFilterType::Pointer imageFilter =
    SpatialObjectToImageFilterType::New();
 
  //  The SpatialObjectToImageFilter requires that the user defines the grid
  //  parameters of the output image. This includes the number of pixels along
  //  each dimension, the pixel spacing, image direction and
  imageFilter->SetSize( inImage->GetLargestPossibleRegion().GetSize() );
  imageFilter->SetSpacing( inImage->GetSpacing() );
  imageFilter->SetOrigin( inImage->GetOrigin() );
  imageFilter->SetDirection( inImage->GetDirection() );
 
  imageFilter->SetUseObjectValue( true );
  imageFilter->SetOutsideValue( 0 );
  imageFilter->SetInput(group);
  
  itk::SimpleFilterWatcher watcher(imageFilter, "ObjectToImageFilter");
//  watcher.QuietOn();

  typedef itk::ImageFileWriter< ImageType >     WriterType;
  WriterType::Pointer writer = WriterType::New();
 
  writer->SetFileName( argv[2] );
  writer->SetInput( imageFilter->GetOutput() );
 
  try
  {
    imageFilter->Update();
    writer->Update();
  }
  catch( itk::ExceptionObject & excp )
  {
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
