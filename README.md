
[SVStools](https://bitbucket.org/bloylChop/svstool)
====================================================

This package contains a simple c++ tool for converting
[Siemens single voxel spectroscopy](https://www.healthcare.siemens.com/magnetic-resonance-imaging/options-and-upgrades/clinical-applications/single-voxel-spectroscopy)
dicoms into nifti image masks to allow comparision with other imaging modalities.

Interested parties should also investigate
[imScribe](http://cmroi.med.upenn.edu/imscribe/) a matlab program for image prescription.

Requirments
-----------
1. [ITK](http://itk.org/ITK/resources/software.html)
2. [cmake](http://cmake.org/cmake/resources/software.html)

Installation (linux)
------------
1. download the software to './SVStool-src'
2. make a build directory
    'mkdir ./SVStool-build'
    'cd ./SVStool-build'
3. Configure
    'ccmake ../SVStool-src'
  specify the ITK directory where itk was installed. Alternatively specify the environment variable ITK_DIR
    'press c' twice
4. Build it
    'make'
  there should now be a ./bin/SVS_voxelToMask executable

Running it
----------
###calling syntax :
    SVS_voxelToMask templateFile outputImageFile dcmFile1 [dcmFile2 dcmFile3 ...]

###parameters :
    templateFile    : structural file from the same scan as the SVS acquisition. Output mask will have this geometry.
    outputImageFile : file to save output mask to.
    dcmFile1        : dicom file of first SVS voxel. (will be labeled a 1 in the output mask)
    dcmFile2 ...    : addtional SVS dicoms


